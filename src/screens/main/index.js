import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { Container, Content, Button, Text } from "native-base";
import styles from "./styles";

export default class Main extends Component {
  render() {
    return (
      <Container>
        <StatusBar
          barStyle="light-content"
        />
        <Content style={styles.box}>
          <Button
            style={{ marginTop: 10 }}
            onPress={() => this.props.navigation.navigate('Login')}
            block>
            <Text>Login</Text>
          </Button>
          <Button
            style={{ marginTop: 10 }}
            onPress={() => this.props.navigation.navigate('Signup')}
            block>
            <Text>Signup</Text>
          </Button>
        </Content>
      </Container>
    )
  }
};