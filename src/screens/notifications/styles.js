import { Dimensions } from 'react-native';

const imgWidth = Dimensions.get('screen').width / 2 - 4;

export default {
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  box: {
    width: imgWidth,
    margin: 2,
  },
  img: {
    height: imgWidth,
    width: imgWidth,
  },
  text: {
    fontSize: 10,
  },
  subtext: {
    fontSize: 9,
    color: '#666',
  },
  thumbnail: {
    height: 40,
    width: 40,
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 50,
  },
  container: {
    flex: 1
  },
  imageContent: {
      flexDirection: 'row',
      margin: 16
  },
  movieContent: {
      margin: 8,
      justifyContent: 'space-between',
      flexDirection: 'column'
  },
  imagew: {
      width: 80,
      height: 80,
      borderRadius: 5,
  },
  mcontent: {
      marginTop: 8,
      marginBottom: 8
  },
}