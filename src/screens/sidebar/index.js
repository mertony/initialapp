import React, { Component } from "react";
import { Alert, AsyncStorage } from 'react-native';
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge,
  Header,
  Body,
  Button,
  Title,
  Thumbnail,
} from "native-base";
import styles from "./style";

const datas = [
  {
    name: "Home",
    route: "Stack",
    icon: "home",
    bg: "#C5F442"
  },
  {
    name: "Questions",
    route: "Stack",
    icon: "rss-square",
    bg: "#477EEA",
  },
  {
    name: "Post",
    route: "Stack",
    icon: "edit",
    bg: "#477EEA",
    types: "11"
  },
  {
    name: "Rewards",
    route: "Stack",
    icon: "comments",
    bg: "#477EEA",
  },
  {
    name: "Profile",
    route: "Main",
    icon: "user",
    bg: "#477EEA",
  },
];

const logo = require("../../../assets/crowdception.png");

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4
    };
  }

  render() {
    return (
      <Container>
        <Header rounded>
          <Left>
            <Thumbnail small source={logo} />
          </Left>
          <Body>
            <Title>Menu</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon type="FontAwesome" name="exchange" onPress={ () => this.props.navigation.toggleDrawer() }/>
            </Button>
          </Right>
        </Header>
        <Content
          bounces={false}
          style={{ flex: 1, backgroundColor: "#fff", top: -1 }}
        >
          <List
            dataArray={datas}
            renderRow={data =>
              <ListItem
                button
                noBorder
                onPress={() => this.props.navigation.navigate(data.route)}
              >
                <Left>
                  <Icon
                    active
                    type="FontAwesome"
                    name={data.icon}
                    style={{ color: "#777", fontSize: 26, width: 30 }}
                  />
                  <Text style={styles.text}>
                    {data.name}
                  </Text>
                </Left>
                {data.types &&
                  <Right style={{ flex: 1 }}>
                    <Badge
                      style={{
                        borderRadius: 3,
                        height: 25,
                        width: 72,
                        backgroundColor: data.bg
                      }}
                    >
                      <Text
                        style={styles.badgeText}
                      >{`${data.types} Types`}</Text>
                    </Badge>
                  </Right>}
              </ListItem>}
          />
          <List>
            <ListItem
              button
              noBorder
              onPress={this._logout}
            >
              <Left>
                <Icon
                  active
                  type="FontAwesome"
                  name="sign-out"
                  style={{ color: "#777", fontSize: 26, width: 30 }}
                />
                <Text style={styles.text}>
                  Logout
                </Text>
              </Left>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }

  _logout = () => {
    Alert.alert(
      'Logout',
      'Are you sure you want to logout?',
      [
        {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Yes', onPress: () => {
          AsyncStorage.removeItem('socialtoken');
          this.props.navigation.navigate('Main');
        }},
      ],
      { cancelable: false }
    )
    this.props.navigation.closeDrawer();
  }
}

export default SideBar;
