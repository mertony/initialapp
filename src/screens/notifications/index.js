import React, { Component } from 'react';
import {
  Content,
  List,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Right,
  Body,
  Separator,
  Icon,
} from 'native-base';
import { StatusBar, Image, View, ScrollView, RefreshControl, TouchableOpacity } from 'react-native';
import styles from "./styles";

const logo = require("../../../assets/crowdception.png");

export default class Home extends Component
{
  state = {
    prod: [],
    tempProd: [],
    refreshing: false,
    notifications: [
      {
        note: 'John Doe, there are 8 new answers for you',
        time: '10',
      },
      {
        note: "Its time to build a difference . .",
        time: "10",
      },
      {
        note: "One needs courage to be happy and smiling all time . . ",
        time: "10",
      },
      {
        note: 'John Doe, there are 8 new answers for you',
        time: "10",
      },
      {
        note: "Its time to build a difference . .",
        time: '10',
      },
      {
        note: "One needs courage to be happy and smiling all time . . ",
        time: '10',
      },
      {
        note: 'John Doe, there are 8 new answers for you',
        time: '10',
      },
      {
        note: "Its time to build a difference . .",
        time: '10',
      },
      {
        note: "One needs courage to be happy and smiling all time . . ",
        time: '10',
      },
      {
        note: "One needs courage to be happy and smiling all time . . ",
        time: '10',
      },
    ]
  }

  constructor(props)
  {
    super(props);
  }

  componentDidMount()
  {
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
  }

  render()
  {
    return (
      <Content>
        <Separator bordered>
          <Text style={{textAlign: 'center', fontSize: 15, textAlignVertical: 'center'}}>Total Rewards Earned: 100  <Icon type="FontAwesome" name="thumbs-up" style={{textAlign: 'center', fontSize: 15}}/></Text>
        </Separator>
        <List
            dataArray={this.state.notifications}
            renderRow={item =>
              <ListItem avatar>
                <Left>
                  <Thumbnail small source={logo} />
                </Left>
                <Body>
                  <Text>
                    {item.note}
                  </Text>
                </Body>
                <Right>
                  <Text note>
                    {item.time}
                  </Text>
                  <Icon type="FontAwesome" name="thumbs-up"/>
                </Right>
              </ListItem>}
          />
      </Content>
    )
  }

}