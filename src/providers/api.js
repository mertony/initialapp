import {create} from 'apisauce';
import { APP_CONFIG } from '../config/global';
import { AsyncStorage, Alert } from 'react-native';
// import RNFetchBlob from 'rn-fetch-blob';

const api = create({
    baseURL: APP_CONFIG.BASE_URL,
    headers: {'Accept': 'application/json',
              'Content-Type': 'application/json'}
  })

export const data = {
    
    get : async function (url, data = {}){
        const token = await AsyncStorage.getItem('socialtoken');
        if (token !== null){
            api.setHeaders({'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token});
        };
        return new Promise((resolve,reject)=>{
            api.get(url, JSON.stringify(data))
            .then((res) => {
                resolve(res.data);
            })
            .catch((err)=>{ 
                console.error('Error', err)
                 reject(response);
            })
        })
    },

    post : async function (url, data = {}){
        const token = await AsyncStorage.getItem('socialtoken');
        if (token !== null){
            api.setHeaders({'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token});
        };
        return new Promise((resolve,reject)=>{
            api.post(url, JSON.stringify(data)).then((res) => {
                if(res.status == 200) {
                    resolve(res);
                } else {
                    reject(this.errorReponseHandler(res));
                }
            }).catch(err => {
                
            });
        })
    },

    timer: function (){
        return new Promise((resolve,reject)=>{
            setTimeout(()=>{
                resolve('Hello Darkness my old friend')
            },5000)
        })
        // setInterval(()=>{
        // },1000)
    },

    upload: async function (url, img, info){
        const token = await AsyncStorage.getItem('socialtoken');
        return new Promise((resolve, reject) => {
            RNFetchBlob.fetch('POST', APP_CONFIG.BASE_URL+url, {
                Authorization : "Bearer " + token,
                otherHeader : "foo",
                'Content-Type' : 'multipart/form-data',
            }, [
                { name : 'image', filename : 'image.png', type:'image/png', data: img},
                { name : 'info', data : JSON.stringify(info)},
            ]).then((resp) => {
                resolve(resp);
            }).catch((err) => {
                reject(err);
            })
        })
    },

    errorReponseHandler: function (err){
        console.log(err);
        switch (err.status) {
        //   case 400: // Invalid token
        //     AsyncStorage.removeItem('socialtoken');
        //     setTimeout(() => {
        //       const root  = this.app.getRootNav();
        //       root.popToRoot();
        //     }  , 1000);
    
        //     this.presentToast(err.data.error)
        //     break;
    
        //   case 401: // Expired Token
        //     AsyncStorage.removeItem('socialtoken');
        //     setTimeout(() => {
        //       try {
        //         const root  = this.app.getRootNav();
        //         root.popToRoot();
        //       } catch (error) {
                
        //       }
        //     }, 1000);
    
        //     this.presentToast(err.data.error)
        //     break;
    
          case 422:
            this.presentAlert(err.data.error);
        }
        return err;
    },

    presentToast: function (msg) {
        // let toast = this.toastCtrl.create({
        //     message: msg,
        //     duration: 3000,
        //     position: 'bottom',
        //     dismissOnPageChange: true,
        //     showCloseButton: true,
        //     closeButtonText: 'close',
        // });
        // toast.onDidDismiss(() => {
        //     console.log('Dismissed toast');
        // });
        // toast.present();
    },

    presentAlert: function (errMsg) {
        console.log(errMsg);
        var msg = '';
        for(var propt in errMsg){
          for(let i=0; i<errMsg[propt].length; i++){
            msg += errMsg[propt][i] + '\n';
          }
        }
        Alert.alert(
            'Error Message',
            msg,
            [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
        )
    }
}