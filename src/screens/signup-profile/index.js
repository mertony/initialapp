import React, { Component } from 'react';
import { Container, Header, Body, Left, Right, Title, Button, Icon } from 'native-base';
import allReducers from '../../reducers';
import {createStore} from 'redux';
import { Provider } from 'react-redux';
import ProfileForm from './profile-form';
const store = createStore(allReducers);

export default class SignupProfile extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    let params = this.props.navigation.state.params;
    params['nav'] = this.props.navigation;
    return (
      <Provider store= {store}>
        <Container>
          <Header>
            <Left>
              <Button transparent onPress={() => this.props.navigation.goBack()}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body>
              <Title>Signup Form</Title>
            </Body>
            <Right/>
          </Header>
          <ProfileForm initialValues={params}/>
        </Container>
      </Provider>
    );
  }

}
