import React , { Component } from 'react';
import { TouchableOpacity, View, AsyncStorage } from 'react-native';
import { Item, Input, Content, Button, Text, Label, Thumbnail, Picker, DatePicker } from 'native-base';
import { Field, reduxForm } from 'redux-form';
import { data } from '../../providers/api';
// import ImagePicker from 'react-native-image-picker';
import styles from "./styles";

const options = {
  quality: 1.0,
  maxWidth: 500,
  maxHeight: 500,
  title: 'Select Photo',
  storageOptions: {
    skipBackup: true,
    // path: 'images',
  },
};

const validate = values => {

  const error = {};
  error.email = '';
  error.first_name = '';
  error.last_name = '';

  var ema = values.email;
  var fn = values.first_name;
  var ln = values.last_name;

  if(values.email === undefined) {
    ema = '';
  }

  if(values.first_name === undefined) {
    fn = '';
  }

  if(values.last_name === undefined) {
    ln = '';
  }

  if(ema.length < 4 && ema !== '') {
    error.email = 'too short';
  }

  if(!ema.includes('@') && ema !== '') {
    error.email = '@ not included';
  }

  if(fn.length > 16) {
    error.first_name = 'max 16 characters';
  }

  if(ln.length > 16) {
    error.last_name = 'max 16 characters';
  }

  return error;
};

class ProfileForm extends Component {
  constructor(props) {
    super(props);
    console.log('inside form');
    console.log(props);
    console.log(this.state);
    this.state={
      // picture: {uri: props.initialValues.picture},
      gender: "Female",
      birthdate: new Date()
    };
  }
  
  renderInput = ({ input, label, type, meta: { touched, error, warning } }) => {
    var hasError= false;
    if(error !== undefined) {
      hasError= true;
    }
    return(
      <View>
        <Item floatingLabel error= {hasError}>
          <Label>{label}</Label>
          <Input {...input}/>
        </Item>
        {hasError ? <Text style={styles.error}>{error}</Text> : <Text />}
      </View>
    )
  }
  
  render() {
    const { handleSubmit, reset } = this.props;
    return (
      <Content>
        <TouchableOpacity style={styles.image}>
          <Thumbnail style={styles.thumbnail} source={this.state.picture} />
        </TouchableOpacity>
        <Content padder>
          <Field name="email" label="Email" component={this.renderInput} />
          <Field name="first_name" label="Firstname" component={this.renderInput} />
          <Field name="last_name" label="Lastname" component={this.renderInput} />
          <View style={{borderBottomWidth: 0.5, borderBottomColor: '#aaa', marginTop: -10}}>
            <Text style={{color: '#666', marginLeft: 2, marginBottom: -8, fontSize: 17, fontFamily: 'none'}}>Gender</Text>
            <Picker
              mode="dropdown"
              selectedValue={this.state.gender}
              onValueChange={this._genderPick}
            >
              <Item label="Female" value="Female" />
              <Item label="Male" value="Male" />
            </Picker>
          </View>
          <View style={{borderBottomWidth: 0.5, borderBottomColor: '#aaa', marginTop: 17}}>
            <Text style={{color: '#666', marginLeft: 2, marginBottom: -5, fontSize: 17, fontFamily: 'none'}}>Birthdate</Text>
            <DatePicker
              textStyle={{fontFamily: 'none'}}
              placeHolderTextStyle={{fontFamily: 'none'}}
              locale={"en"}
              modalTransparent={false}
              animationType={"fade"}
              androidMode={"default"}
              placeHolderText="Select date"
              onDateChange={this._setDate}
            />
          </View>
          <Button style={{marginTop: 30}} block primary onPress= {handleSubmit(this.signUp)}>
            <Text>Submit</Text>
          </Button>
        </Content>
      </Content>
    )
  }

  signUp = values => {
    console.log('Calling this. reacting')
    let params = values;
    params['gender'] = this.state.gender;
    params['birthdate'] = this.state.birthdate;
    let nav = params['nav'];
    delete params['nav'];
    alert(JSON.stringify(params));
    console.log(params);
    // data.post('sign-up', params).then((res)=>{
    //   console.log('Products result', res)
    //   try {
    //     AsyncStorage.setItem('socialtoken', res.data.token);
    //     nav.navigate('Tabs');
    //   } catch (error) {
    //     // Error saving data
    //   }
    // }).catch((err)=>{
      
    // })
  }

  _getImages = () => {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
    
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
    
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
    
        this.setState({
          picture: source,
        });
      }
    })
  }

  _genderPick = value => {
    this.setState({
      gender: value
    });
  }

  _setDate = newDate => {
    this.setState({ birthdate: newDate });
  }
}

export default reduxForm({
  form: 'profileForm',
  validate
})(ProfileForm)