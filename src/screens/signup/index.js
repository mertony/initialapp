import React, { Component } from 'react';
import { Alert, View } from 'react-native';
import { Container, Content, Header, Title, Left, Right, Body,Button, Text, Icon } from "native-base";
import { LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { statusCodes, GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
// import { data } from '../../providers/api';
import styles from "./styles";

export default class Signup extends Component {

  constructor(props)
  {
    super(props);
    this._handleGooglesignIn = this._handleGooglesignIn.bind(this);
  }

  async componentDidMount() {
    this._configureGoogleSignIn();
    this._handleGooglesignOut();
  }

  _configureGoogleSignIn() {
    GoogleSignin.configure({
      webClientId: '1065738929096-lf1ugd614rkr85bl6gi8afpihmsq7s3l.apps.googleusercontent.com',
      offlineAccess: false,
    });
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Signup</Title>
          </Body>
          <Right />
        </Header>

        <Content style={styles.box}>
          <Button
            style={{ marginTop: 10, backgroundColor: '#3F51B5' }}
            onPress={this._handleFacebookLogin}
            block>
            <Icon active name="logo-facebook" />
            <Text style={{ width: 214 }}>Signup with Facebook</Text>
          </Button>
          <Button
            style={{ marginTop: 10 }}
            onPress={this._handleGooglesignIn}
            block danger>
            <Icon active name="logo-google" />
            <Text style={{ width: 214 }}>Signup with Google</Text>
          </Button>
        </Content>
      </Container>
    );
  }

  _handleFacebookLogin = () => {
    LoginManager.logInWithReadPermissions(["email","user_friends"]).then(
      data => {
        console.log(JSON.stringify(data));
        const infoRequest = new GraphRequest(
          '/me?fields=id,name,email,first_name,last_name,picture.width(320).height(320)',
          null,
          this._responseInfoCallback,
        );
        new GraphRequestManager().addRequest(infoRequest).start();
      },
      error => {
        console.log('Login fail with error: ' + error)
      }
    );
  }

  _responseInfoCallback = (error, result) => {
    if (error) {
      console.log('Error fetching data: ' + JSON.stringify(error));
    } else {
      console.log('Success fetching data: ' + JSON.stringify(result));
      this.props.navigation.navigate('SignupProfile', {id: result.id, email: result.email, first_name: result.first_name, last_name: result.last_name, picture: result.picture.data.url});
    }
  }

  _handleGooglesignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo);
      this.props.navigation.navigate('SignupProfile', {id: userInfo.user.id, email: userInfo.user.email, first_name: userInfo.user.givenName, last_name: userInfo.user.familyName, picture: userInfo.user.photo});
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // sign in was cancelled
        Alert.alert('cancelled');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation in progress already
        Alert.alert('in progress');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        Alert.alert('play services not available or outdated');
      } else {
        Alert.alert('Something went wrong', error.toString());
      }
    }
  };
  
  _handleGooglesignOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
    } catch (error) {
      console.log(error);
    }
  };

  // _register()
  // {
  //   console.log('Calling this. reacting')
  //   data.post('guest-search-name-product', {offset: 0, search: ''}).then((res)=>{
  //     console.log('Products result', res)
  //   }).catch((err)=>{
      
  //   })
  // }
}
