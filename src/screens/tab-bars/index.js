import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Title, Icon, Tabs, Tab, TabHeading, Text, Thumbnail} from "native-base";
import styles from "./styles";
import Home from '../home';
import Feeds from '../feeds';
import Post from '../post';
import Notifications from '../notifications';
const logo = require("../../../assets/crowdception.png");
export default class TabBars extends Component {
  render() {
    return (
      <Container>
        <Header rounded>
          <Left>
            <Button transparent>
              <Icon type="FontAwesome" name="search" onPress={ () => this.props.navigation.goBack() }/>
            </Button>
          </Left>
          <Body>
            <Title rounded>
              <Thumbnail small source={logo} /> CrowdCeption
            </Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon type="FontAwesome" name="bars" onPress={ () => this.props.navigation.toggleDrawer() }/>
            </Button>
          </Right>
        </Header>

        <Tabs lazy="false">
          <Tab heading=
            {
              <TabHeading >
                <Icon name="home" type="FontAwesome"/>
              </TabHeading>
            }>
            <Home tabnav={this.props.navigation} navigation={this.props.navigation}/>
          </Tab>
          <Tab heading=
            {
              <TabHeading >
                <Icon name="rss-square" type="FontAwesome"/>
              </TabHeading>
            }>
            <Feeds tabnav={this.props.navigation} navigation={this.props.navigation}/>
          </Tab>
          <Tab heading=
            {
              <TabHeading >
                <Icon name="edit" type="FontAwesome"/>
              </TabHeading>
            }>
            <Post tabnav={this.props.navigation} navigation={this.props.navigation}/>
          </Tab>
          <Tab heading=
            {
              <TabHeading >
                <Icon name="comments" type="FontAwesome"/>
              </TabHeading>
            }>
            <Notifications tabnav={this.props.navigation} navigation={this.props.navigation}/>
          </Tab>
        </Tabs>


      </Container>
    )
  }
};