import React, { Component } from 'react';
import { Card, 
  CardItem, 
  Body, 
  Text, 
  Left, 
  Thumbnail, 
  Content, 
  Button, 
  Title, 
  Textarea,
  Container,
  Header,
  Icon,
  Right,
  Input,
  Footer,
} from 'native-base';
import { StatusBar, Image, View, ScrollView, RefreshControl, TouchableOpacity } from 'react-native';
import styles from "./styles";
import { data } from '../../providers/api';

export default class Post extends Component
{
  state = {
    user_id:1,
  }

  constructor(props)
  {
    super(props);
  }

  componentDidMount()
  {
  }

  updateValue(text, field) {
    this.setState({[field]:text}); 
  }

  submit = () => {
    console.log('this.state');
    console.log(this.state);
    data.post('question', this.state).then((res)=>{
      console.log('Products result', res)
      alert(res.data.message);
    }).catch((err)=>{
      console.log(err);
    })
  }

  render()
  {
    return (
      <Container>
        <Content padder style={styles.content}>
          <Input onChangeText={(text) => this.updateValue(text, 'title')} placeholder="Title" placeholderTextColor="#999999" style={styles.forminput}/>
          <Text style={styles.formtext}> Enter your question for CrowdCeption: </Text>
          <Textarea onChangeText={(text) => this.updateValue(text, 'description')}  rowSpan={12} bordered style={styles.forminput}/>
        </Content>
        <Footer>
          <Right>
            <Button rounded style={styles.formbutton} onPress={() => this.submit()}>
              <Text style={styles.formbuttontext}>Submit</Text>
            </Button>
          </Right>
        </Footer>
      </Container>
    )
  }

}