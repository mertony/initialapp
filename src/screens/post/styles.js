import { Dimensions } from 'react-native';

const imgWidth = Dimensions.get('screen').width / 2 - 4;

export default {
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  box: {
    width: imgWidth,
    margin: 2,
  },
  img: {
    height: imgWidth,
    width: imgWidth,
  },
  text: {
    fontSize: 10,
  },
  subtext: {
    fontSize: 9,
    color: '#666',
  },
  thumbnail: {
    height: 40,
    width: 40,
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 50,
  },
  formtext: {
    color:'#888888', 
    paddingTop: 20,
  },
  forminput: {
    backgroundColor: 'white', 
    borderRadius: 5, 
    borderColor: '#d9d9d9', 
    color: '#777777',
  },
  formbutton: {
    backgroundColor:'white', 
    borderRadius:50, 
    marginRight:10, 
    padding:10,
  },
  formbuttontext: {
    color: 'blue'
  },
  content: {
    backgroundColor: '#e5e5e5',
    padding: 10,
  },
}