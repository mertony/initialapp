import React, { Component } from 'react';
import {
  Container,
  Content,
  Thumbnail,
  Icon,
  Button,
  Card,
  CardItem,
  Body
} from 'native-base';
import { 
  View,
  Text,
  FlatList,
  TouchableHighlight,
  RefreshControl,
  ScrollView,
  Image,
} from 'react-native';
import styles from "./styles";
import { data } from '../../providers/api';
import { APP_CONFIG } from '../../config/global';
import Shimmer from '../../components/Shimmer';

const skeleton = require("../../../assets/skeleton.jpg");
const face = require("../../../assets/face.png");
const logo = require("../../../assets/crowdception.png");

export default class Feeds extends Component
{
  state = {
    question: [],
    tempQuestion: [],
    refreshing: false,
    tweets: [
      {
      user: {
        avatar: "https://i.redd.it/u05ph6fm3ab21.jpg",
        name: "Juan dela Cruz",
      },
      tweetContent: "This girl I used to pick on through school told me she thinks I am a pathetic person when I told her I really regret the way I use to treat her. Do you think it was rude what she did to me?",
      replies: 3,
      followers: 1,
      likes:7,
    },
    {
      user: {
        avatar: "../../../assets/skeleton.jpg",
        name: "Blue miming",
      },
      tweetContent: "The only people my sister chooses to socialize with is her boyfriend and her best friend who is a girl. Do you think my sister is anti-social?",
      replies: 3,
      followers: 1,
      likes:7,
    },
    {
      user: {
        avatar: "../../../assets/face.jpg",
        name: "Black miming",
      },
      tweetContent: "What is the greatest movie plot hole of all time?",
      replies: 3,
      followers: 1,
      likes:7,
    },
    {
      user: {
        avatar: "../../../assets/face.jpg",
        name: "Black dog",
      },
      tweetContent: "Question here?",
      replies: 3,
      followers: 1,
      likes:7,
    },
  ]
  }

  constructor(props)
  {
    super(props);
  }

  componentWillMount()
  {
    this._makeTempHolder();
    this._getQuestions();
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    this._getQuestions();
  }

  _makeTempHolder()
  {
    let temp = [];
    for (let index = 0; index < 10; index++) {
      temp.push(
        <Card style={styles.mb}>
          <CardItem>
            <View style={styles.tweet}>
              <TouchableHighlight
                underlayColor="white"
                activeOpacity={0.75}
              >
                <View style={styles.imageContent}>
                <Shimmer autoRun={true} style={styles.imagew}>
                    <Image style={styles.imagew}
                        source={skeleton}></Image>
                </Shimmer>
                  <View
                    style={styles.movieContent}
                  >
                    <Shimmer autoRun={true}>
                        <Text>Question Title here ...</Text>
                    </Shimmer>
                    <Shimmer autoRun={true}>
                        <Text>by name</Text>
                    </Shimmer>

                  </View>
                </View>
              </TouchableHighlight>
            </View>
          </CardItem>
        </Card>
      )
    }
    this.setState({tempQuestion: temp});
  }

  _getQuestions() {
    console.log('Calling this. reacting')
    data.get('question').then((res)=>{
      var question = res.data.questions.map((prop, key) => {
        return (
          <Card key={key} style={styles.mb}>
            <CardItem>
                <View style={styles.tweet}>
                  <TouchableHighlight
                    underlayColor="white"
                    activeOpacity={0.75}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <Thumbnail source={face} />
                      <View
                        style={{
                          flexDirection: "column",
                          justifyContent: "flex-start"
                        }}
                      >
                        <Text
                          style={{
                            paddingLeft: 15,
                            fontWeight: "bold",
                            fontSize: 20
                          }}
                        >
                          {prop.title}
                        </Text>

                        <Text
                          style={{
                            paddingLeft: 15,
                            color: "#aaa",
                            fontSize: 16
                          }}
                        >
                          {"@" + prop.name}
                        </Text>
                      </View>
                    </View>
                  </TouchableHighlight>
                  <Text style={styles.tweetText}>{prop.description}</Text>
                  <View style={styles.tweetFooter}>
                    <View style={styles.footerIcons}>
                      <Button transparent dark>
                        <Icon type="FontAwesome" name="thumbs-up" />
                        <Text style={styles.badgeCount}>1</Text>
                      </Button>
                    </View>
                    <View style={styles.footerIcons}>
                      <Button
                        transparent
                        dark
                      >
                        <Icon type="FontAwesome" name="comments" />
                        <Text style={styles.badgeCount}>2</Text>
                      </Button>
                    </View>
                    <View style={styles.footerIcons}>
                      <Button transparent dark>
                        <Icon type="FontAwesome" name="rss" />
                        <Text style={styles.badgeCount}>3</Text>
                      </Button>
                    </View>
                  </View>
                </View>
            </CardItem>
          </Card>
        );
      });
      this.setState({question: question, refreshing: false});
      console.log('Questions result', res)
    }).catch((err)=>{
      this.setState({refreshing: false});
    })
  }

  render()
  {
    return (
      <ScrollView
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
        />
      }>
        <View style={styles.content}>
          { this.state.question == '' ? this.state.tempQuestion : this.state.question }
        </View>
      </ScrollView>
    )
  }

 
}