import { Dimensions } from 'react-native';

const btnH = 100;
const topMrgn = Dimensions.get('window').height / 2 - btnH;

export default {
  box: {
    marginTop: topMrgn,
    marginHorizontal: 20,
  },
}