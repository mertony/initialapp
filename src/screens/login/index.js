import React, { Component } from 'react';
import { Alert, AsyncStorage } from 'react-native';
import { Container, Content, Header, Title, Left, Right, Body,Button, Text, Icon } from "native-base";
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { statusCodes, GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import { data } from '../../providers/api';
import styles from "./styles";

export default class Login extends Component {

  constructor(props)
  {
    super(props)
  }

  async componentDidMount() {
    this._configureGoogleSignIn();
    this._handleGooglesignOut();
  }

  _configureGoogleSignIn() {
    GoogleSignin.configure({
      webClientId: '807538150675-2m5cidatcvnovbla0qmtnf9rk5p0nmav.apps.googleusercontent.com',
      offlineAccess: false,
    });
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Login</Title>
          </Body>
          <Right />
        </Header>

        <Content style={styles.box}>
          <Button
            style={{ marginTop: 10, backgroundColor: '#3F51B5' }}
            onPress={this._handleFacebookLogin}
            block>
            <Icon active name="logo-facebook" />
            <Text style={{ width: 214 }}>Login with Facebook</Text>
          </Button>
          <Button
            style={{ marginTop: 10 }}
            onPress={this._handleGooglesignIn}
            block danger>
            <Icon active name="logo-google" />
            <Text style={{ width: 214 }}>Login with Google</Text>
          </Button>
          <Button
            style={{ marginTop: 10, backgroundColor: '#fff', borderWidth: 0.5 }}
            onPress={this._loginAsGuest}
            block>
            <Text style={{ width: 214, color: '#000', textAlign: "center" }}>Login as Guest</Text>
          </Button>
        </Content>
      </Container>
    );
  }

  _handleFacebookLogin = () => {
    // this.props.navigation.navigate('Welcome');
    LoginManager.logInWithReadPermissions(["email","user_friends"]).then(
      data => {
        AccessToken.getCurrentAccessToken().then(
          (data) => {
            this._login(data.userID);
            console.log(JSON.stringify(data));
          }
        )
      },
      error => {
        console.log('Login fail with error: ' + error)
      }
    );
  }

  _handleGooglesignIn = async () => {
    // this.props.navigation.navigate('Welcome');
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo);
      this._login(userInfo.user.id);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // sign in was cancelled
        Alert.alert('cancelled');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation in progress already
        Alert.alert('in progress');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        Alert.alert('play services not available or outdated');
      } else {
        Alert.alert('Something went wrong', error.toString());
      }
    }
  };
  
  _handleGooglesignOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
    } catch (error) {
      console.log(error);
    }
  };

  _loginAsGuest = () => {
    this.props.navigation.navigate('TabBars');
  }

  _login = id => {
    console.log('Logging in');
    this.props.navigation.navigate('TabBars');
    // data.post('login', {id: id}).then((res)=>{
    //   console.log('Dashboard result', res.data.token);
    //   try {
    //     AsyncStorage.setItem('socialtoken', res.data.token);
    //     this.props.navigation.navigate('Tabs');
    //   } catch (error) {
    //     // Error saving data
    //   }
    // }).catch((err)=>{
    //   // console.error('Dashboard error', err);
    // })
  }
}
