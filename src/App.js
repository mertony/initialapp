import React from "react";
import { Root} from "native-base";
import { createAppContainer, createStackNavigator, createDrawerNavigator } from "react-navigation";

import SideBar from "./screens/sidebar";
import TabBars from "./screens/tab-bars/"
import Main from "./screens/main/";
import Login from "./screens/login/";
import Signup from "./screens/signup/";
import SignupProfile from "./screens/signup-profile";
import Details from "./screens/details";

const Stack = createStackNavigator(
  {
    Main: { screen: Main },
    TabBars: { screen: TabBars },
    Signup: { screen: Signup },
    SignupProfile: { screen: SignupProfile },
    Details: { screen: Details},
  },
  {
    initialRouteName: "Main",
    headerMode: "none",
  }
);

const Drawer = createDrawerNavigator(
  {
    Stack: { screen: Stack },
    Main: { screen: Main },
    Login: { screen: Login },
  },
  {
    initialRouteName: "Stack",
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    contentComponent: props => <SideBar {...props} />
  }
)

const AppNavigator = createAppContainer(Drawer);

export default () =>
  <Root>
    <AppNavigator />
  </Root>;